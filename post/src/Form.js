import React,{useRef} from "react";
import axios from "axios";
import Button from 'react-bootstrap/Button'

function Form(){
    const name=useRef("")
    const password=useRef("")
    const postData=(e)=>{
            const data={name:name.current.value,
                password:password.current.value
            }
            axios.post('https://jsonplaceholder.typicode.com/posts', data)
              .then(function (response) {
                console.log(response);
                alert("Data Posted")
              })
              .catch(function (error) {
                console.log(error);
              });
        e.preventDefault();

    }
    return(
        <div className="main">
            <h2>User Login</h2>
            <form onSubmit={postData}>
                <label>User Name: </label>
                <input type="text" placeholder="Enter User Name" ref={name}></input>
                <label>Password: </label>
                <input type="password" placeholder="Enter Password" ref={password}></input>
                <div className="btn">
                <button type="submit">Submit</button>
               </div>
                
            </form>
        </div>
       
    )
}

export default Form;